import React, { FC } from "react";
import { ButtonProps } from "./ButtonTypes";

import "./Button.style.css";

const CustomButton: FC<ButtonProps> = ({ title, ...props }) => {
  return (
    <button
      className={props.className ? props.className : "custom-button-style"}
      {...props}
    >
      {title}
    </button>
  );
};

export default CustomButton;
